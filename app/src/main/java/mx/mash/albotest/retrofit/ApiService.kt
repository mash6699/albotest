package mx.mash.albotest.retrofit

import io.reactivex.Observable
import mx.mash.albotest.constants.AppConstants
import mx.mash.albotest.model.Beer
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET(AppConstants.API_BEERS)
    fun requestBeers(@Query("page") currentPage: Int, @Query("per_page") pageSize: Int): Observable<ArrayList<Beer>>

}