package mx.mash.albotest.constants

object AppConstants {

    const val API_HOST = "https://api.punkapi.com/v2/"
    const val API_BEERS = "beers"

    const val BEER = "BEER"

}