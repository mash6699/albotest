package mx.mash.albotest.ui.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import mx.mash.albotest.listener.ServiceListener
import mx.mash.albotest.model.Beer
import mx.mash.albotest.repository.ServiceRepository


class HomeViewModel : ViewModel(), ServiceListener {

    private val TAG = this::class.java.simpleName
    private val serviceRepository = ServiceRepository()
    var beeList = MutableLiveData<List<Beer>>()

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    fun getBeers(page: Int, size: Int) {
        serviceRepository.callBeers(page, size).subscribe(this::onSuccess, this::onFailed)
    }

    override fun onSuccess(beerList: ArrayList<Beer>) {
        uiScope.launch {
            beeList.value = beerList
        }
    }

    override fun onFailed(throwable: Throwable) {
        Log.d(TAG, "onFailed" + throwable.message)
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}
