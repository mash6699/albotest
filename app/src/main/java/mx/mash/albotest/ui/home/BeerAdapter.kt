package mx.mash.albotest.ui.home


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import mx.mash.albotest.R
import mx.mash.albotest.databinding.BeerItemBinding
import mx.mash.albotest.listener.ItemListener
import mx.mash.albotest.model.Beer


class BeerAdapter (val listener: ItemListener): RecyclerView.Adapter<BeerAdapter.BeerViewHolder>() {

    private var beerList: ArrayList<Beer> = ArrayList()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): BeerViewHolder {
        val beerListItemBinding: BeerItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.context), R.layout.beer_item, viewGroup, false)
        return BeerViewHolder(beerListItemBinding)
    }

    override fun onBindViewHolder(beerViewHolder: BeerViewHolder, i: Int) {
        val beer: Beer = beerList!![i]
        beerViewHolder.beerItemBinding.beer = beer
        beerViewHolder.itemView.findViewById<CardView>(R.id.cv).setOnClickListener{
            listener.onClick(beer)
        }
    }

    override fun getItemCount(): Int {
        return if (beerList != null) { beerList!!.size } else { 0 }
    }

    fun setBeerList(beerList: List<Beer>) {
        this.beerList?.addAll(beerList)
        notifyDataSetChanged()
    }

    inner class BeerViewHolder(beerItemBinding: BeerItemBinding) : RecyclerView.ViewHolder(beerItemBinding.root) {
        val beerItemBinding: BeerItemBinding = beerItemBinding
    }

    fun clear() {
        beerList = ArrayList()
        notifyDataSetChanged()
    }

}