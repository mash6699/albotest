package mx.mash.albotest.ui.detail


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import mx.mash.albotest.R
import mx.mash.albotest.constants.AppConstants
import mx.mash.albotest.databinding.DetailFragmentBinding
import mx.mash.albotest.model.Beer


class BeerDetailFragment : Fragment() {

    private val TAG = this::class.java.simpleName
    private lateinit var binding: DetailFragmentBinding
    lateinit var beer: Beer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            beer = it.getSerializable(AppConstants.BEER) as Beer
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DetailFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.beer = beer
    }

}
