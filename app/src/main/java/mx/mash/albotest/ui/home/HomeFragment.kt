package mx.mash.albotest.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import mx.mash.albotest.R
import mx.mash.albotest.constants.AppConstants
import mx.mash.albotest.databinding.HomeFragmentBinding
import mx.mash.albotest.listener.ItemListener
import mx.mash.albotest.model.Beer

class HomeFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener, ItemListener {

    private val TAG = this::class.java.simpleName
    private val beerAdapter: BeerAdapter = BeerAdapter(this)
    private val viewModel = HomeViewModel()
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var binding: HomeFragmentBinding


    private var PAGE_START = 1
    private val SIZE_PAGE  = 20
    private val MAX_PAGES = 5


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = HomeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipeRefresh = binding.swipeRefresh
        recyclerView = binding.rvBeers

        recyclerView.adapter = beerAdapter
        recyclerView.layoutManager = LinearLayoutManager(context)

        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.setHasFixedSize(true)
        swipeRefresh.setOnRefreshListener(this)
        viewModel.getBeers(PAGE_START, SIZE_PAGE)

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if(PAGE_START < MAX_PAGES) {
                        PAGE_START += 1
                        viewModel.getBeers(PAGE_START , SIZE_PAGE)
                        Snackbar.make(recyclerView, getString(R.string.lbl_load_data),  Snackbar.LENGTH_SHORT).show()
                    } else {
                        Snackbar.make(recyclerView, getString(R.string.lbl_msg_page_max),  Snackbar.LENGTH_SHORT).show()
                    }
                }
            }
        })

        viewModel.beeList.observe(viewLifecycleOwner, Observer {
            beerAdapter.setBeerList(it)
            swipeRefresh.isRefreshing = false

        })
    }

    override fun onRefresh() {
        beerAdapter.clear()
        PAGE_START = 1
        viewModel.getBeers(PAGE_START, SIZE_PAGE)
    }

    override fun onClick(beer: Beer) {
        try {
            var bundle = Bundle()
            bundle.putSerializable(AppConstants.BEER, beer)
            findNavController().navigate(R.id.detailFragment, bundle)
        } catch (e: Exception){
            e.printStackTrace()
        }
    }

}
