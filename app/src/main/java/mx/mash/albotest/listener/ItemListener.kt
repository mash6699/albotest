package mx.mash.albotest.listener

import mx.mash.albotest.model.Beer

interface ItemListener {
    fun onClick(beer: Beer)
}