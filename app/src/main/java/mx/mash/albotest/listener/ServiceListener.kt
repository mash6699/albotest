package mx.mash.albotest.listener

import mx.mash.albotest.model.Beer

interface ServiceListener {
    fun onSuccess(beerList: ArrayList<Beer>)
    fun onFailed(throwable: Throwable)
}