package mx.mash.albotest.util

import android.util.Log
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import mx.mash.albotest.R

object AdapterUtil {

    @BindingAdapter("imageUrl")
    @JvmStatic
    fun loadImage(imageView: ImageView, imageUrl: String?) {
        Log.d("URL", "URL-> $imageUrl")
        Glide.with(imageView.context)
            .setDefaultRequestOptions(RequestOptions().circleCrop())
            .load(imageUrl)
            .placeholder(R.drawable.ic_beer)
            .into(imageView)
    }

}