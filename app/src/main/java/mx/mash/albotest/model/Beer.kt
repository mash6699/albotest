package mx.mash.albotest.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class Beer(@SerializedName("id") var id: Int?,
                @SerializedName("name") var name: String?,
                @SerializedName("tagline") var tagline: String?,
                @SerializedName("first_brewed") var firstBrewed: String?,
                @SerializedName("description") var description: String?,
                @SerializedName("image_url") var imageUrl: String?,
                @SerializedName("abv") var abv: Double?,
                @SerializedName("ibu") var ibu: Double?,
                @SerializedName("target_fg") var targetFg: Double?,
                @SerializedName("target_og") var targetOg: Double?,
                @SerializedName("ebc") var ebc: Double?,
                @SerializedName("srm") var srm: Double?,
                @SerializedName("ph") var ph: Double?,
                @SerializedName("attenuation_level") var attenuationLevel: Double?,
                @SerializedName("volume") var volume: Volume,
                @SerializedName("boil_volume") var boilVolume: Volume,
                @SerializedName("method") var method: Method,
                @SerializedName("ingredients") var ingredients: Ingredients?,
                @SerializedName("food_pairing") var foodPairing: ArrayList<String>?,
                @SerializedName("brewers_tips") var brewersTips: String?,
                @SerializedName("contributed_by") var contributedBy: String?): Serializable {

    fun content(): String {
        return "TAGLINE: " + tagline + "\n\nDESCRIPTION: "+ description + "\n\nFIRST BREWED: " + firstBrewed +  "\n\nFOOD PAIRING: " + foods()
    }

    fun foods(): String{
        var content: String = ""
        if(foodPairing != null){
            for(s in this!!.foodPairing!!){
                content += "\n-$s"
            }
        }
        return content
    }

}

data class Volume(@SerializedName("value") var value: Double?,
                  @SerializedName("unit") var unit: String?): Serializable

data class Amount(@SerializedName("value") var value: Double?,
                  @SerializedName("unit") var unit: String?): Serializable

data class Method(@SerializedName("mash_temp") var mashTemp: ArrayList<MashTemp>,
                  @SerializedName("fermentation") var fermentation: MashTemp,
                  @SerializedName("twist") var twist: String?): Serializable

data class MashTemp(@SerializedName("temp") var temp: Volume?,
                    @SerializedName("duration") var duration: Double?): Serializable

data class Malt(@SerializedName("name") var name: String?,
                @SerializedName("amount") var amount: Amount?): Serializable

data class Hops(@SerializedName("name") var name: String?,
                @SerializedName("amount") var amount: Amount?,
                @SerializedName("add") var add: String?,
                @SerializedName("attribute") var attribute: String?): Serializable

data class Ingredients(@SerializedName("malt") var malt: ArrayList<Malt>,
                       @SerializedName("hops") var hops: ArrayList<Hops>?): Serializable



