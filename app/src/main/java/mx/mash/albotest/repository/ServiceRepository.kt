package mx.mash.albotest.repository

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import mx.mash.albotest.model.Beer
import mx.mash.albotest.retrofit.ApiService
import mx.mash.albotest.retrofit.RetrofitClient

class ServiceRepository {

    private val apiService: ApiService = RetrofitClient.getRetrofit().create(ApiService::class.java)

    fun callBeers(page: Int, size: Int): Observable<ArrayList<Beer>> {
        return apiService.requestBeers(page, size).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
    }

}